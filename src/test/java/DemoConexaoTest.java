import java.sql.*;

public class DemoConexaoTest {


    public static void main(String[] args) throws SQLException {
        Connection c;
         //ResultSet rs = null;
        String url = "jdbc:oracle:thin:@172.23.10.17:1521:coredb";
        String usuario = "AUTOMACAO_COMERCIAL";
        String senha = "kombat#12";
        String sql = "select * from ag0307.ccrppaco";

        try {;
            // 1º estabelecer a conexao
            Class.forName("oracle.jdbc.driver.OracleDriver");
            c = DriverManager.getConnection(url, usuario, senha);
            System.out.println("Conectou com sucesso");
            PreparedStatement stm = c.prepareStatement(sql);
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getString("FDDTAOPEN"));
            }

            rs.close();
            stm.close();
            c.close();
            System.out.println("Fechou a conexão");
        } catch (SQLException e) {
            System.out.println("Erro ao conectar: " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("Erro ao registrar: " + e.getMessage());
        }
    }

}
