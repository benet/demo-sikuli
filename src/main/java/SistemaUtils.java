import org.sikuli.script.*;

import java.io.File;
import java.util.ArrayList;

public class SistemaUtils {

    private static Pattern pattern = new Pattern();
    private static Screen screen = new Screen();
    private static String path =  System.getProperty("user.dir")+"/src/test/img/";


    public void abrirSistema(String codSistema, String codAgencia){
        try{
            pattern.setImage(Image.create(path+"bat.png"));
            screen.doubleClick(pattern);

            pattern.setImage(Image.create(path+"select_system"));
            screen.wait(pattern, 30);
            screen.type(codSistema);
            screen.type(Key.ENTER);

            pattern.setImage(Image.create(path+"select_system_agencia"));
            screen.wait(pattern, 10);
            screen.type(codAgencia);
            screen.type(Key.ENTER);





        }catch (Exception e){

            e.getCause();

        }

    }

    public void logarSistema(){
        try {

            pattern.setImage(Image.create(path+"view_login"));
            screen.wait(pattern, 30);
            screen.type("JULIANOJ179");
            screen.type(Key.ENTER);

            pattern.setImage(Image.create(path+"label_password"));
            screen.wait(pattern, 10);
            screen.type("teste123");
            screen.type(Key.ENTER);



        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }


    }


    public void selecionarMenu(ArrayList<String> menu) {


        try {
            pattern.setImage(Image.create(path+"label_select_printer"));
            screen.wait(pattern, 30);
            screen.type(Key.ENTER);

        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }

        for(int pos=0; pos < menu.size();pos++){
            String select = menu.get(pos);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            screen.type(select);

        }
    }




}
